#!/usr/bin/env bash

########################
# include the magic
########################
. ./demo-magic/demo-magic.sh


########################
# Configure the options
########################

#
# speed at which to simulate typing. bigger num = faster

TYPE_SPEED=100

#
# custom prompt
#
# see http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html for escape sequences
#
DEMO_PROMPT="${GREEN}➜ ${CYAN}\W "

# text color
# DEMO_CMD_COLOR=$BLACK

# hide the evidence
clear

# set up color var
red=$(tput setaf 1)

echo "${red} Vault Advanced Data Protection on AWS EKS Project"
#### Terraform Init ####
####                ####
echo "${red} Initializing Terraform Modules...."
cd ~/ADP-Project/ADP
pe "terraform init"

#### EKS Module #### 
echo "${red} Provisioning resources...."
pe "terraform apply --auto-approve"


