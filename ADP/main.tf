## General Locals ## 
## Change according to your needs ## 
locals {
  name       = "Yarden-ADP"
  vpc-name   = "Yarden-VPC"
  cluster_name   = "eks-Yarden"
  region = "us-east-1"
  tags = {
    Owner       = "Yardenw@Terasky.com"
    Environment = "HashiCorpCompetency"
  }
}

data "aws_caller_identity" "current" {}

### VPC Module ### 
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  name = local.vpc-name
  cidr = "20.3.0.0/16"

  azs                 = ["${local.region}a", "${local.region}b", "${local.region}c"]
  private_subnets     = ["20.3.1.0/24", "20.3.2.0/24", "20.3.3.0/24"]
  public_subnets      = ["20.3.11.0/24", "20.3.12.0/24", "20.3.13.0/24"]
  # Tags used for Subnets auto discovery when deploying aws-load-balancer-controller 
  public_subnet_tags = {
    "kubernetes.io/role/elb" = "1"
  }
  
  create_elasticache_subnet_group = false
  create_redshift_subnet_group = false

  manage_default_network_acl = true
  default_network_acl_tags   = { Name = "${local.name}-default" }

  manage_default_route_table = true
  default_route_table_tags   = { Name = "${local.name}-default" }

  manage_default_security_group = true
  default_security_group_tags   = { Name = "${local.name}-default" }

  enable_dns_hostnames = true
  enable_dns_support   = true

  ## Use Single Nat Gateway for all AZs ## 
  enable_nat_gateway = true
  single_nat_gateway = true

  tags = local.tags
}

### EKS Module ### 
module "eks" {
  # https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest
  source  = "terraform-aws-modules/eks/aws"

  cluster_name    = local.cluster_name
  cluster_version = "1.21"

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets
  cluster_endpoint_private_access = true

  create_cluster_security_group = true
  create_node_security_group    = true
  cluster_tags = local.tags

  cluster_addons = {
    coredns = {
      resolve_conflicts = "OVERWRITE"
    }
    kube-proxy = {}
    vpc-cni = {
      resolve_conflicts        = "OVERWRITE"
      service_account_role_arn = module.vpc_cni_irsa.iam_role_arn
    }
  }

# Cluster Security Group Rules # 
  cluster_security_group_additional_rules = {
    egress_nodes_ephemeral_ports_tcp = {
      description                = "To node 1025-65535"
      protocol                   = "tcp"
      from_port                  = 1025
      to_port                    = 65535
      type                       = "egress"
      source_node_security_group = true
    }
  }
  node_security_group_additional_rules = {
    ingress_self_all = {
      description = "Node to node all ports/protocols"
      protocol    = "-1"
      from_port   = 0
      to_port     = 0
      type        = "ingress"
      self        = true
    }
    ingress_cluster_all = {
      description                   = "Cluster to node all ports/protocols"
      protocol                      = "-1"
      from_port                     = 0
      to_port                       = 0
      type                          = "ingress"
      source_cluster_security_group = true
    }
    egress_all = {
      description      = "Node all egress"
      protocol         = "-1"
      from_port        = 0
      to_port          = 0
      type             = "egress"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }

  eks_managed_node_groups = {
    Yarden-NG = {
      instance_types = ["t3.medium"]
      create_security_group = true
      min_size     = 1
      max_size     = 1
      desired_size = 1
    }
  }
}
# Allow Nodes to join the cluster
module "vpc_cni_irsa" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "~> 4.12"

  role_name_prefix      = "VPC-CNI-IRSA"
  attach_vpc_cni_policy = true
  vpc_cni_enable_ipv4   = true

  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-node"]
    }
  }
}

## Helm Charts Module ##
## Contains aws-load-balancer-controller, Vault & MySQL Charts with custom Values ##   
module "eks-essentials" {
  depends_on      = [module.eks]
  source          = "./terraform-modules/helm-charts"
  # Please Change this to your license
  vault_license   = ""
}

# ## ECR Repository Creation
## If you didn't create an ECR yet, Please un comment this resource. 
# resource "aws_ecr_repository" "Yarden-ADP-Project" {
#   depends_on           = [module.eks-essentials]
#   name                 = "yarden-adp"
#   image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
#   tags = local.tags
# }

data "aws_lb" "app-ingress" {
  depends_on = [module.eks-essentials]
  tags = {
    "elbv2.k8s.aws/cluster" = local.cluster_name 
  }
}

output "LoadBalancerName" {
  value = data.aws_lb.app-ingress.dns_name
}

# In Order to wait for the Load Balancer to be available
resource "time_sleep" "wait_180_seconds" {
  depends_on = [module.eks-essentials]
  create_duration = "180s"
}

# Vault Module ###
module "vault" {
  depends_on = [resource.time_sleep.wait_180_seconds]
  source           = "./terraform-modules/vault"
}

## ECR Image Data Source ##
## Please change the name to your ECR Repo in order to fetch the url ## 
data "aws_ecr_repository" "yarden-adp" {
  name = "yarden-adp"
}

## Application Deployment Module ##  
# Change yarden-adp in the image value in order to use the correct data source # 
module "app" {
  depends_on = [module.vault]
  source = "./terraform-modules/app"
  image  = "${data.aws_ecr_repository.yarden-adp.repository_url}:v2"
}