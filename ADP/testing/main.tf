## This Folder is for local testing on minikube environment
### Generates a Kubernetes Secret to hold vault enterprise license key ## 
resource "kubernetes_secret" "vault-license" {
  metadata {
    name = "vault-license"
    namespace = "default"
  }

  data = {
    license = ""
  }

  type = "Opaque"
}

## Deploys Vault Enterpeise Helm Chart with Extra Values ## 
resource "helm_release" "vault" {
  name       = "vault"
  depends_on = [resource.kubernetes_secret.vault-license]
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  values     = [
    file("${path.module}/vault-values.yaml")
  ]
}

## Deploys MySql Helm Chart With Extra Values ## 
resource "helm_release" "mysql" {
  name       = "mysql"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  values     = [
    file("${path.module}/mysql-values.yaml")
  ]
}

# Vault Module ###
module "vault" {
  source           = "../terraform-modules/vault"
}

### Apply Kubernetes Deployment ### 
resource "kubernetes_deployment" "flask-api" {
  metadata {
    name = "flask-api"
    labels = {
      app = "flask-api"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "flask-api"
      }
    }

    template {
      metadata {
        labels = {
          app = "flask-api"
        }
      }

      spec {
        service_account_name = "auth"
        container {
          image = "yarden-adp:v1"
          name  = "flask-api"
          image_pull_policy = "Never"
        }
      }
    }
  }
}
