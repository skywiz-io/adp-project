## For Local Testing ! ## 
provider "vault" {
    token = "root"
    address = "http://127.0.0.1:8200"
    skip_child_token = true
    add_address_to_env = false
}

provider "kubernetes" {
  # For Local Testing! #
  config_path    = "~/.kube/config"
}

provider "helm" {
  kubernetes {
  # For Local Testing! #
  config_path    = "~/.kube/config"
  }
}