# Mount Transform 
resource "vault_mount" "mount_transform" {
  path = "transform"
  type = "transform"
}

# Create a Transform role 
resource "vault_transform_role" "payments" {
  path = vault_mount.mount_transform.path
  name = "payments"
  transformations = ["card-number"]
}

# Create a Transformation
resource "vault_transform_transformation" "card-number" {
  path          = vault_mount.mount_transform.path
  name          = "card-number"
  type          = "fpe"
  template      = "builtin/creditcardnumber"
  tweak_source  = "internal"
  allowed_roles = ["payments"]
}

# Policy for Transform 
resource "vault_policy" "secrets-policy" {
  name = "secrets-policy"

  policy = <<EOT
# To request data encoding using any of the roles
# Specify the role name in the path to narrow down the scope
path "transform/encode/*" {
   capabilities = [ "update" ]
}

# To request data decoding using any of the roles
# Specify the role name in the path to narrow down the scope
path "transform/decode/*" {
   capabilities = [ "update" ]
}
EOT
}

# Apply Role to Kubernetes Auth 
resource "vault_kubernetes_auth_backend_role" "transform_role" {
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "payments"
  bound_service_account_names      = ["auth"]
  bound_service_account_namespaces = ["default"]
  token_ttl                        = 3600
  token_policies                   = ["secrets-policy"]
}