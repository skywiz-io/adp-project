
# Create a tokenization role 
resource "vault_transform_role" "idnumber" {
  path = vault_mount.mount_transform.path
  name = "mobile-pay"
  transformations = ["idnumber"]
}

# Create a Transformation
resource "vault_transform_transformation" "idnumber" {
  path          = vault_mount.mount_transform.path
  name          = "idnumber"
  type          = "tokenization"
  allowed_roles = ["mobile-pay"]
}

# Policy for Tokenize 
resource "vault_policy" "tokenize-policy" {
  name = "tokenize-policy"

  policy = <<EOT
path "transform/encode/mobile-pay" {
   capabilities = [ "update" ]
}

# To request data decoding using any of the roles
# Specify the role name in the path to narrow down the scope
path "transform/decode/mobile-pay" {
   capabilities = [ "update" ]
}

# To validate the token
path "transform/validate/mobile-pay" {
   capabilities = [ "update" ]
}

# To retrieve the metadata belong to the token
path "transform/metadata/mobile-pay" {
   capabilities = [ "update" ]
}

# To check and see if the secret is tokenized
path "transform/tokenized/mobile-pay" {
   capabilities = [ "update" ]
}
EOT
}

# Apply Tokenize Role to Kubernetes Auth 
resource "vault_kubernetes_auth_backend_role" "tokenize_role" {
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "mobile-pay"
  bound_service_account_names      = ["auth"]
  bound_service_account_namespaces = ["default"]
  token_ttl                        = 3600
  token_policies                   = ["tokenize-policy"]
}
