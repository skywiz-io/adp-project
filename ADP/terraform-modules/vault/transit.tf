
### TRANSIT ### 
# Enable transit secret engine #
resource "vault_mount" "mount_transit" {
  path = "transit"
  type = "transit"
}

# Generate Key
resource "vault_transit_secret_backend_key" "key" {
  backend = vault_mount.mount_transit.path
  name    = "transit_key"
}

# Generate a policy for the key
resource "vault_policy" "transit-key-policy" {
  name = "transit-key-policy"

  policy = <<EOT
path "transit/encrypt/${resource.vault_transit_secret_backend_key.key.name}" {
   capabilities = [ "update" ]
}

path "transit/decrypt/${resource.vault_transit_secret_backend_key.key.name}" {
   capabilities = [ "update" ]
}
EOT
}

# Apply Role to Kubernetes Auth 
resource "vault_kubernetes_auth_backend_role" "transit_role" {
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "transit_key"
  bound_service_account_names      = ["auth"]
  bound_service_account_namespaces = ["default"]
  token_ttl                        = 3600
  token_policies                   = ["transit-key-policy"]
}