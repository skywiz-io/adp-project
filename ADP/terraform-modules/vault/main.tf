
# Sets up Cluster_Role_Binding # 
# Permissions to verify JWT using auth sa
resource "kubernetes_cluster_role_binding" "cluster_role_binding" {
  metadata {
    name = "role-tokenreview-binding"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "system:auth-delegator"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "auth"
    namespace = "default"
  }
}

# Sets up a Kubernetes Service Account as the verificator # 
resource "kubernetes_service_account" "auth" {
  depends_on=[resource.kubernetes_cluster_role_binding.cluster_role_binding]
  metadata {
    name = "auth"
  }
}

# Sets up kubernetes auth method # 
resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}


# Get SA_JWT_TOKEN 
data "kubernetes_secret" "token_reviewer_jwt" {
  metadata {
    name      = resource.kubernetes_service_account.auth.default_secret_name
    namespace = "default"
  }
  binary_data = {
    token = ""
  }
}

# Get SA_CA_CRT 
data "kubernetes_secret" "ca_crt" {
  metadata {
    name      = resource.kubernetes_service_account.auth.default_secret_name
    namespace = "default"
  }
  binary_data = {
    "ca.crt" = ""
  }
}

# Configures K8S Auth Method
resource "vault_kubernetes_auth_backend_config" "k8s" {
  depends_on = [data.kubernetes_secret.ca_crt, data.kubernetes_secret.token_reviewer_jwt]
  backend                = vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://10.96.0.1:443"
  kubernetes_ca_cert     = base64decode(data.kubernetes_secret.ca_crt.binary_data["ca.crt"])
  token_reviewer_jwt     = base64decode(data.kubernetes_secret.token_reviewer_jwt.binary_data.token)
  disable_iss_validation = "true"
}
