### Apply Kubernetes Deployment ### 
resource "kubernetes_deployment" "flask-api" {
  metadata {
    name = "flask-api"
    labels = {
      app = "flask-api"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "flask-api"
      }
    }

    template {
      metadata {
        labels = {
          app = "flask-api"
        }
      }

      spec {
        service_account_name = "auth"
        container {
          image = var.image
          name  = "flask-api"
          image_pull_policy = "Always"
        }
      }
    }
  }
}