import mysql.connector

# Create Database using cursor # 
mydb = mysql.connector.connect(
    host="mysql.default.svc.cluster.local",
    user="root",
    passwd="Password",
    )

my_cursor = mydb.cursor()

my_cursor.execute("CREATE DATABASE data")

my_cursor.execute("SHOW DATABASES")
for db in my_cursor:
    print(db)