from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from itsdangerous import base64_decode
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from sqlalchemy_utils.functions import database_exists
import hvac
import sys
import base64

app = Flask(__name__)

## Helper for Transit ##
def base64ify(bytes_or_str):
    """Helper method to perform base64 encoding across Python 2.7 and Python 3.X"""
    if sys.version_info[0] >= 3 and isinstance(bytes_or_str, str):
        input_bytes = bytes_or_str.encode('utf8')
    else:
        input_bytes = bytes_or_str

    output_bytes = base64.urlsafe_b64encode(input_bytes)
    if sys.version_info[0] >= 3:
        return output_bytes.decode('ascii')
    else:
        return output_bytes

# Configure DB Connection # 
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Password@mysql.default.svc.cluster.local:3306/data'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
app.config['SECRET_KEY'] = 'Secret'

# Init DB # 
db = SQLAlchemy(app)

# Create DB Model - Transform # 
class Transform(db.Model):
    string = db.Column(db.String(200), primary_key=True, nullable=False)
    creditcard = db.Column(db.String(200))

    def _repr_(self):
        return '<Name %r>' % self.name

# Create DB Model - Transform Decode # 
class TransformDecode(db.Model):
    decoded_cc = db.Column(db.String(200), primary_key=True, nullable=False)

    def _repr_(self):
        return '<Name %r>' % self.name

# Create DB Model - Tokenize # 
class Tokenize(db.Model):
    string = db.Column(db.String(200), primary_key=True, nullable=False)
    idnumber = db.Column(db.String(200))

    def _repr_(self):
        return '<Name %r>' % self.name

# Create DB Model - Tokenize Decode # 
class TokenizeDecode(db.Model):
    decoded_id = db.Column(db.String(200), primary_key=True, nullable=False)

    def _repr_(self):
        return '<Name %r>' % self.name

# Create DB Model - Transit # 
class Transit(db.Model):
    string = db.Column(db.String(200), primary_key=True, nullable=False)
    transit = db.Column(db.String(200))

    def _repr_(self):
        return '<Name %r>' % self.name

# Create DB Model - Transit Decode # 
class TransitDecode(db.Model):
    decoded_transit = db.Column(db.String(200), primary_key=True, nullable=False)

    def _repr_(self):
        return '<Name %r>' % self.name
    
# Create DB If it Doesn't exists # 
if database_exists(app.config["SQLALCHEMY_DATABASE_URI"]):
    print('Database Already Exists!')
else:
    import create_db

from app import db
db.create_all()

@app.route('/')
def home():
    return "Hello!"

## FORMS ## 
class TransformForm(FlaskForm):
    string = StringField("Enter a String", validators=[DataRequired()])
    creditcard = StringField("Enter a Credit Card Number - Transform Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class TransformDecodeForm(FlaskForm):
    decoded_cc = StringField("Enter the encoded Credit Card Number - Transform Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class TokenizeForm(FlaskForm):
    string = StringField("Enter a String", validators=[DataRequired()])
    idnumber = StringField("Enter an ID Number - Tokenize Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class TokenizeDecodeForm(FlaskForm):
    decoded_id = StringField("Enter the encoded ID Number - Tokenize Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class TransitForm(FlaskForm):
    string = StringField("Enter a String", validators=[DataRequired()])
    transit = StringField("Enter a Credit Card Number - Transit Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class TransitDecodeForm(FlaskForm):
    decoded_transit = StringField("Enter the encoded Credit Card Number - Transit Method", validators=[DataRequired()])
    submit = SubmitField("Submit")
 

## TRANSFORM ENCODE ##
@app.route('/transform-encode', methods=['GET', 'POST'])
def transform():
    string = None
    creditcard = None
    form = TransformForm()
    # Validate the form # 
    # Transform Encode # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With Payments Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_payments = client.auth_kubernetes(
        jwt=jwt,
        role="payments",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_payments['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'payments'
        transformation_name = 'card-number'
        transformations = [transformation_name]
        # Use the role/transformation to encode a value
        encode_response_cc = client.secrets.transform.encode(
            role_name=role_name,
            value=form.creditcard.data,
            transformation=transformation_name,
        )
        transform = Transform(string=form.string.data, creditcard=encode_response_cc['data']['encoded_value'])
        db.session.add(transform)
        db.session.commit()

    encoded_values = Transform.query
    return render_template("transform-encode.html", form=form, encoded_values=encoded_values)
    
## TRANSFORM DECODE ##
@app.route('/transform-decode', methods=['GET', 'POST'])
def transform_decode():
    decoded_cc = None
    form = TransformDecodeForm()
    # Validate the form # 
    # Transform Decode # 
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With Payments Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_payments = client.auth_kubernetes(
        jwt=jwt,
        role="payments",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_payments['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'payments'
        transformation_name = 'card-number'
        transformations = [transformation_name]
        # Use the role/transformation to decode a value
        decode_response_cc = client.secrets.transform.decode(
            role_name=role_name,
            value=form.decoded_cc.data,
            transformation=transformation_name,
        )
        transform_decode = TransformDecode(decoded_cc=decode_response_cc['data']['decoded_value'])
        db.session.add(transform_decode)
        db.session.commit()

    decoded_values = TransformDecode.query
    return render_template("transform-decode.html", form=form, decoded_values=decoded_values)


## TOKENIZE ENCODE ## 
## ENCODE ##
@app.route('/tokenize-encode', methods=['GET', 'POST'])
def tokenize():
    string = None
    idnumber = None
    form = TokenizeForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With mobile-pay Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_mobilepay = client.auth_kubernetes(
        jwt=jwt,
        role="mobile-pay",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_mobilepay['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'mobile-pay'
        transformation_name = 'idnumber'
        transformations = [transformation_name]
        # Use the role/transformation to encode a value
        encode_response_id = client.secrets.transform.encode(
            role_name=role_name,
            value=form.idnumber.data,
            transformation=transformation_name,
        )
        tokenize = Tokenize(string=form.string.data, idnumber=encode_response_id['data']['encoded_value'])
        db.session.add(tokenize)
        db.session.commit()

    encoded_values = Tokenize.query
    return render_template("tokenize-encode.html", form=form, encoded_values=encoded_values)
    
## TOKENIZE DECODE ##  
@app.route('/tokenize-decode', methods=['GET', 'POST'])
def tokenize_decode():
    string = None
    decoded_id = None
    form = TokenizeDecodeForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With mobile-pay Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_mobilepay = client.auth_kubernetes(
        jwt=jwt,
        role="mobile-pay",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_mobilepay['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'mobile-pay'
        transformation_name = 'idnumber'
        transformations = [transformation_name]
        # Use the role/transformation to decode a value
        decode_response_id = client.secrets.transform.decode(
            role_name=role_name,
            value=form.decoded_id.data,
            transformation=transformation_name,
        )
        tokenize_decode = TokenizeDecode(decoded_id=decode_response_id['data']['decoded_value'])
        db.session.add(tokenize_decode)
        db.session.commit()

    decoded_values = TokenizeDecode.query
    return render_template("tokenize-decode.html", form=form, decoded_values=decoded_values)

## TRANSIT ENCODE ## 
@app.route('/transit-encode', methods=['GET', 'POST'])
def transit():
    string = None
    transit = None
    form = TransitForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With transit-key Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_transit_key = client.auth_kubernetes(
        jwt=jwt,
        role="transit_key",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_transit_key['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        text=form.transit.data
        # Use Transit to Encode a Value from The Form
        encrypt_data_response = client.secrets.transit.encrypt_data(
        name='transit_key',
        plaintext=base64ify(text.encode()),
        )
        transit = Transit(string=form.string.data, transit=encrypt_data_response['data']['ciphertext'])
        db.session.add(transit)
        db.session.commit()

    encoded_values = Transit.query
    return render_template("transit-encode.html", form=form, encoded_values=encoded_values)
 
## TRANSIT DECODE ## 
   
## DECODE ## 
@app.route('/transit-decode', methods=['GET', 'POST'])
def transit_decode():
    decoded_transit = None
    form = TransitDecodeForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With transit-key Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_transit_key = client.auth_kubernetes(
        jwt=jwt,
        role="transit_key",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_transit_key['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        cipher=form.decoded_transit.data
        # Use Transit to Encode a Value from The Form
        decrypt_data_response = client.secrets.transit.decrypt_data(
        name='transit_key',
        ciphertext=form.decoded_transit.data,
        )

        base64plaintext = decrypt_data_response['data']['plaintext']
        plaintext = base64_decode(base64plaintext)
        transit_decode = TransitDecode(decoded_transit=plaintext)
        db.session.add(transit_decode)
        db.session.commit()

    decoded_values = TransitDecode.query
    return render_template("transit-decode.html", form=form, decoded_values=decoded_values)

app.run(host="0.0.0.0", port=8000)

