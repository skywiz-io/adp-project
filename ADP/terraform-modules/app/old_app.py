from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from itsdangerous import base64_decode
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from sqlalchemy_utils.functions import database_exists
import hvac
import sys
import base64

app = Flask(__name__)

## Helper for Transit ##
def base64ify(bytes_or_str):
    """Helper method to perform base64 encoding across Python 2.7 and Python 3.X"""
    if sys.version_info[0] >= 3 and isinstance(bytes_or_str, str):
        input_bytes = bytes_or_str.encode('utf8')
    else:
        input_bytes = bytes_or_str

    output_bytes = base64.urlsafe_b64encode(input_bytes)
    if sys.version_info[0] >= 3:
        return output_bytes.decode('ascii')
    else:
        return output_bytes

# Configure DB Connection # 
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Password@mysql.default.svc.cluster.local:3306/data'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
app.config['SECRET_KEY'] = 'Secret'

# Init DB # 
db = SQLAlchemy(app)

# Create DB Model # 
class Data(db.Model):
    string = db.Column(db.String(200), primary_key=True, nullable=False)
    creditcard = db.Column(db.String(200))
    idnumber = db.Column(db.String(200))
    transit = db.Column(db.String(200))

    def _repr_(self):
        return '<Name %r>' % self.name
    
# Create DB Model # 
class Decode(db.Model):
    decoded_cc = db.Column(db.String(200), primary_key=True, nullable=False)
    decoded_id = db.Column(db.String(200))
    decoded_transit = db.Column(db.String(200))

    def _repr_(self):
        return '<Name %r>' % self.name
    
# Create DB If it Doesn't exists # 
if database_exists(app.config["SQLALCHEMY_DATABASE_URI"]):
    print('Database Already Exists!')
else:
    import create_db

from app import db
db.create_all()

@app.route('/')
def home():
    return "Hello!"

## FORMS ## 
class DataForm(FlaskForm):
    string = StringField("Enter a String", validators=[DataRequired()])
    creditcard = StringField("Enter a Credit Card Number - Transform Method", validators=[DataRequired()])
    idnumber = StringField("Enter an ID Number - Tokenize Method", validators=[DataRequired()])
    transit = StringField("Enter a Credit Card Number - Transit Method", validators=[DataRequired()])
    submit = SubmitField("Submit")

class DecodeForm(FlaskForm):
    decoded_cc = StringField("Enter the encoded Credit Card Number - Transform Method", validators=[DataRequired()])
    decoded_id = StringField("Enter the encoded ID Number - Tokenize Method", validators=[DataRequired()])
    decoded_transit = StringField("Enter the encoded Credit Card Number - Transit Method", validators=[DataRequired()])
    submit = SubmitField("Submit")
 

## ENCODE ##
@app.route('/encode', methods=['GET', 'POST'])
def data():
    string = None
    creditcard = None
    idnumber = None
    transit = None
    form = DataForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With Payments Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_payments = client.auth_kubernetes(
        jwt=jwt,
        role="payments",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_payments['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'payments'
        transformation_name = 'card-number'
        transformations = [transformation_name]
        # Use the role/transformation to encode a value
        encode_response_cc = client.secrets.transform.encode(
            role_name=role_name,
            value=form.creditcard.data,
            transformation=transformation_name,
        )
        # Recieve Token using JWT Verification - With mobile-pay Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_mobilepay = client.auth_kubernetes(
        jwt=jwt,
        role="mobile-pay",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_mobilepay['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'mobile-pay'
        transformation_name = 'idnumber'
        transformations = [transformation_name]
        # Use the role/transformation to encode a value
        encode_response_id = client.secrets.transform.encode(
            role_name=role_name,
            value=form.idnumber.data,
            transformation=transformation_name,
        )
        # Recieve Token using JWT Verification - With transit-key Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_transit_key = client.auth_kubernetes(
        jwt=jwt,
        role="transit_key",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_transit_key['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        text=form.transit.data
        # Use Transit to Encode a Value from The Form
        encrypt_data_response = client.secrets.transit.encrypt_data(
        name='transit_key',
        plaintext=base64ify(text.encode()),
        )
        data = Data(string=form.string.data, creditcard=encode_response_cc['data']['encoded_value'], idnumber=encode_response_id['data']['encoded_value'], transit=encrypt_data_response['data']['ciphertext'])
        db.session.add(data)
        db.session.commit()

    encoded_values = Data.query
    return render_template("encode.html", form=form, encoded_values=encoded_values)
    
## DECODE ## 
@app.route('/decode', methods=['GET', 'POST'])
def decode():
    decoded_cc = None
    decoded_id = None
    decoded_transit = None
    form = DecodeForm()
    # Validate the form # 
    if form.validate_on_submit():
        # Vault Integration
        url = "http://vault:8200"
        client = hvac.Client(url=url)
        # Recieve Token using JWT Verification - With Payments Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_payments = client.auth_kubernetes(
        jwt=jwt,
        role="payments",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_payments['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'payments'
        transformation_name = 'card-number'
        transformations = [transformation_name]
        # Use the role/transformation to decode a value
        decode_response_cc = client.secrets.transform.decode(
            role_name=role_name,
            value=form.decoded_cc.data,
            transformation=transformation_name,
        )
        # Recieve Token using JWT Verification - With mobile-pay Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_mobilepay = client.auth_kubernetes(
        jwt=jwt,
        role="mobile-pay",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_mobilepay['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        role_name = 'mobile-pay'
        transformation_name = 'idnumber'
        transformations = [transformation_name]
        # Use the role/transformation to decode a value
        decode_response_id = client.secrets.transform.decode(
            role_name=role_name,
            value=form.decoded_id.data,
            transformation=transformation_name,
        )
        # Recieve Token using JWT Verification - With transit-key Role # 
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        response_transit_key = client.auth_kubernetes(
        jwt=jwt,
        role="transit_key",
        )
        vault_url = 'http://vault:8200/'
        vault_token = response_transit_key['auth']['client_token']
        ca_path = '/run/secrets/kubernetes.io/serviceaccount/ca.crt'  
        client = hvac.Client(url=vault_url,token=vault_token,verify= ca_path)
        client.is_authenticated()
        cipher=form.decoded_transit.data
        # Use Transit to Encode a Value from The Form
        decrypt_data_response = client.secrets.transit.decrypt_data(
        name='transit_key',
        ciphertext=form.decoded_transit.data,
        )

        base64plaintext = decrypt_data_response['data']['plaintext']
        plaintext = base64_decode(base64plaintext)
        decode = Decode(decoded_cc=decode_response_cc['data']['decoded_value'], decoded_id=decode_response_id['data']['decoded_value'], decoded_transit=plaintext)
        db.session.add(decode)
        db.session.commit()

    decoded_values = Decode.query
    return render_template("decode.html", form=form, decoded_values=decoded_values)



app.run(host="0.0.0.0", port=8000)
