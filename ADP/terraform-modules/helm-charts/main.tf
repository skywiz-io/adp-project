### Generates a Kubernetes Secret to hold vault enterprise license key ## 
resource "kubernetes_secret" "vault-license" {
  metadata {
    name = "vault-license"
    namespace = "default"
  }

  data = {
    license = var.vault_license
  }

  type = "Opaque"
}

## Deploys Vault Enterpeise Helm Chart with Extra Values ## 
resource "helm_release" "vault" {
  name       = "vault"
  depends_on = [resource.kubernetes_secret.vault-license]
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  values     = [
    file("${path.module}/vault-values.yaml")
  ]
}

## Deploys MySql Helm Chart With Extra Values ## 
resource "helm_release" "mysql" {
  name       = "mysql"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  values     = [
    file("${path.module}/mysql-values.yaml")
  ]
}

## Configures aws-load-balancer-controller prereqs ## 
resource "null_resource" "lb-prereqs" {
  depends_on = [resource.helm_release.mysql]
  provisioner "local-exec" {
    command = "sh ${path.module}/alb-prereqs.sh"
  }
}

## Deployes aws-load-balancer-controller ##
resource "helm_release" "aws-load-balancer-controller" {
  name       = "aws-load-balancer-controller"
  namespace  = "default"
  depends_on = [resource.null_resource.lb-prereqs]
  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  values     = [
    file("${path.module}/alb-values.yaml")
  ]
}

# Set Up Ingress # 
resource "kubernetes_ingress_v1" "app-ingress" {
  depends_on = [resource.helm_release.aws-load-balancer-controller]
  metadata {
    name = "app-ingress"
    annotations = {
      "alb.ingress.kubernetes.io/scheme": "internet-facing"
      "alb.ingress.kubernetes.io/target-type": "ip"
      "alb.ingress.kubernetes.io/tags": "Environment=HashiCorp-Competency, Owner=Yarden Weisman"
      "kubernetes.io/ingress.class": "alb"
      "alb.ingress.kubernetes.io/healthcheck-path": "/"
      "alb.ingress.kubernetes.io/success-codes": "200-307"
    }
  }
  spec {
    rule {
      http {
        path {
          path_type = "Prefix"
          path = "/"
          backend {
            service {
              name = "vault"
              port {
                number = 8200
              }
            }
          }
        }
      }
    }
  }
  wait_for_load_balancer = true
}

