aws eks update-kubeconfig --region us-east-1 --name eks-Yarden

eksctl utils associate-iam-oidc-provider --region us-east-1 --cluster eks-Yarden --approve  

# Note: If the Policy already exists on AWS - Feel free to skip this step 
# aws iam create-policy --policy-name AWSLoadBalancerControllerIAMPolicy --policy-document file://iam-policy.json  

eksctl delete iamserviceaccount --cluster=eks-Yarden --namespace=default --name=aws-load-balancer-controller --region us-east-1  

eksctl create iamserviceaccount --cluster=eks-Yarden --namespace=default --name=aws-load-balancer-controller --attach-policy-arn=arn:aws:iam::384617649113:policy/AWSLoadBalancerControllerIAMPolicy --override-existing-serviceaccounts --region us-east-1 --approve

kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller//crds?ref=master"