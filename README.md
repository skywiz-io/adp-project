# HashiCorp Vault on AWS EKS Using Terraform

- [HashiCorp Vault on AWS EKS Using Terraform](#hashicorp-vault-on-aws-eks-using-terraform)
  - [Introduction](#introduction)
  - [Prerequisites](#prerequisites)
  - [Vault's Engines](#vaults-engines)
  - [Application Image Build And Push](#application-image-build-and-push)
  - [Manual Setup](#manual-setup)
  - [Setup Using Demo Magic](#setup-using-demo-magic)
  - [Verification](#verification)
  - [Documentation](#documentation)



## Introduction
This Example presents a demo of settings up an AWS EKS Cluster, Provisioning components on the cluster using Helm Charts,  
Configuring Vault, Deploying different resources of Kubernetes on the cluster and set up an example application that integrates with Vault. 
Everything is being provisioned using Terraform.

## Prerequisites
Terraform - Version 1+  
Kubectl  
AWS CLI  
Helm  
AWS Account  
Hashicorp Vault Enterprise License  
Docker  

## Vault's Engines
Vault is being installed with integrated database (Raft) and in dev mode for this example.  
Also Vault is being installed with Enterprise Image.  
You can change the values of vault-values.yaml according to Hashicorp Vault Helm chart in order to change the configurations.   
  
In this example we will use different methods and engines of HashiCorp Vault:  
For Authentication - Since we're setting up everything on a Kubernetes Cluster, We will use Vault's Kubernetes Auth Method.  
For Advanced Data Protection - We will demonstrate those engines using our sample application:  
**Free-tier Engines:**  
Transit - https://learn.hashicorp.com/tutorials/vault/eaas-transit  
**Enterprise Engines:**  
Transform - https://learn.hashicorp.com/tutorials/vault/transform?in=vault/adp  
Tokenize - https://learn.hashicorp.com/tutorials/vault/tokenization?in=vault/adp  

## Application Image Build And Push
Please make sure to build the docker image before you apply the infratructure of this project,  
In order to do so, Please follow the below instructions:  
Make sure you have an active ECR on your AWS Account.  [You can use the terraform module in order to create one]  
Navigate to ADP/terraform-modules/app - cd ADP/terraform-modules/app  
On your browser, Navigate to your AWS account/ECR.  
You can fetch the pushing commands from the console.  
  
It should look similiar to this:  (From your Terminal)  
  
Build the image locally:  [the dot marks the Dockerfile location]    
docker build -t image_name:version .

Login to your ECR  
aws ecr get-login-password --region us-east-1 | sudo docker login --username AWS --password-stdin your_aws_account  
  
Tag the Image   
sudo docker tag image_name:version ecr_url/image_name:version  
  
Push the image to your ECR  
docker push ecr_url/image_name:version  
  
Verify that the image is created and pushed to your ECR using AWS Console.  

## Manual-Setup
Note: For VPC & EKS We're using the official AWS Modules. 

###### Provision an EKS Cluster
Provisions an EKS cluster - Version 1.21.  

###### Setup aws-load-balancer-controller prerequisites 
The prerequisites are getting installed during the creation of null_resource.lb-prereqs in helm-charts module.  
Please make sure to change the value inside terraform-modules/helm-charts/alb-prereqs.sh to your relevant values before you apply.  

###### Provision Everything With Terraform
Please Run the following command:  
Please Make Sure to apply your Vault Enterprise License Key to main.tf  
terraform apply

This part will provision everything.
AWS VPC & AWS EKS with all the supporting resources.    
Helm-Charts: Vault(Enterprise Version), MySql, aws-load-balancer-controller.  
Kubernetes Resources to support vault.   
Vault's configuration.  
Application Deployment.  

## Setup Using Demo Magic
Demo Magic is a great tool that let's you run commands which pre-coded with just clicking enter.  
In order to use that tool to implement everything on this project, Please run the following command:  
cd /repo-location/  
sh ADP/scripts/demomg.sh  
Note: Please make sure to change the values of terraform-modules/helm-charts/alb-prereqs.sh to your relevant values before you apply.  


## Verification
In order to verify the functionallity of the application, Please do the following:  
First update your local kubeconfig with the following command:  
aws eks update-kubeconfig --region your_region_code --name your_cluster_name  
**For Example**: aws eks update-kubeconfig --region us-east-1 --name eks-yarden   
  
Forward the application traffic to your local pc using the following command:  
kubectl port-forward your_application_pod_name application_port  
**For Example**: kubectl port-forward flask-api 8000  

Open your browser and write the following in the url field:  
http://localhost:8000  
That's the application main page.  
  
**In order to take actions with encoding methods of vault**  
Please go to this url:  
http://localhost:8000/.method_name.-encode  
for example: http://localhost:8000/transform-encode  
Please feel in the form:  
String should be any string. - to show that regular expressions can be pushed to the database without being trafficed through Vault.  
CreditCard - Transform Method -should be in this form: xxxx-xxxx-xxxx-xxxx  
Id Number - Tokenize Method - should be in this form: xxxxxxxxx (9 numbers)  
CreditCard - Transit Method - should be in this form: xxxx-xxxx-xxxx-xxxx  
   
Encoded Values will be displayed in the table below the form once you submit the form.  
  
**In order to take actions with decoding methods of vault**  
Please Copy The Encoded Values from http://localhost:8000/.method_name.-encode table and paste it to the form on the following:  
http://localhost:8000/.method_name.-decode  
for example: http://localhost:8000/tokenize-decode  
Please paste the values to the right boxes in the form:  
Otherwise an exeption will be thrown out.  
  
Decoded Values will be displayed in the table below the form once you submit the form.  
    
**Vault's URL**
Vault server is being accessed using an Application Load Balancer:  
During the Terraform apply run the output of the ALB DNS Record is being presented to you.  
Please paste it to the url field in order to access Vault.  

## Documentation
aws-load-balancer-controller - https://kubernetes-sigs.github.io/aws-load-balancer-controller/v2.2/  
HashiCorp Vault Advaned Data Protection - https://learn.hashicorp.com/collections/vault/adp  
Official AWS Modules - https://github.com/terraform-aws-modules  
hvac python plugin (Integrates between python apps and Vault) - https://hvac.readthedocs.io/en/stable/overview.html  
  
**Helm Charts**  
HashiCorp Vault - https://github.com/hashicorp/vault-helm  
aws-load-balancer-controller - https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller  
Bitnami MySql -  https://github.com/bitnami/charts/tree/master/bitnami/mysql  